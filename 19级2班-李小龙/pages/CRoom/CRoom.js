// pages/CRoom/CRoom.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    slider:4,
    wrodTypeArray:[{typeId: 0, typeName: "全部"},
    {typeId: 1, typeName: "网络流行"},
    {typeId: 2, typeName: "娱乐用语"},
    {typeId: 3, typeName: "游戏动漫"},
    {typeId: 4, typeName: "人文历史"},
    {typeId: 5, typeName: "生活用语"}],
    dictType:-1,
    pingming:3,
    wodi:1,
    baiban:0
  },
/**
 * 
 * switch 
 */
  xuanzhong:function(e){
    var newBaiban=this.data.baiban;
    var NewChecked=e.detail.value;
    var newPinMing=this.data.pingming;
    console.log(NewChecked)
   
    console.log(newPinMing)
    if(NewChecked==false){
      newPinMing=newPinMing+1;
      newBaiban=0;
    }
    if(NewChecked==true){
      newPinMing=newPinMing-1;
      newBaiban=1;
    }
    this.setData({
      pingming:newPinMing,
      baiban:newBaiban
    })
  },
  /**
   * 
   * 滑块绑定的事件。 \
   * 
   * 先获取卧底的值   
   * 总的减去卧底=平民
   */
  changeSlider:function(e){
    var baiban=this.data.baiban;
    var newPinMing=this.data.pingming;
    var newWodi=this.data.wodi;
    var newSilder =e.detail.value;
    if(5<newSilder){
      newWodi=2
    }
    if(9<=newSilder){
      newWodi=3
    }
    newPinMing=newSilder-newWodi-baiban;
   
    this.setData({
      slider:newSilder,
      pingming:newPinMing,
      wodi:newWodi

    })
  },

  pingmingLower:function(e){
   
    this.setData({
      pingming:e.detail.peopleNum,
      wodi:this.data.wodi+1
    })

  
  },
  
  pingmingAdd:function(e){
    this.setData({
      pingming:e.detail.peopleNum,
      wodi:this.data.wodi-1
    })
  },
  wodiLower:function(e){
      this.setData({
        wodi:e.detail.peopleNum,
        pingming:this.data.pingming+1
      })

  },
/**
 *     console.log(e)
 *     输出该函数的一些情况
 */
  wodiAdd:function(e){
    console.log(e)
    this.setData({
      wodi:e.detail.peopleNum,
      pingming:this.data.pingming-1
    })

},

  

  



  dictType:function(e){
    console.log(e.currentTarget.dataset.id )
    this.setData({
      dictType:e.currentTarget.dataset.id
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})