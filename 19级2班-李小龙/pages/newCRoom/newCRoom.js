// pages/newCRoom/newCRoom.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    slider:4,
    wrodTypeArray:[{typeId: 0, typeName: "全部"},
    {typeId: 1, typeName: "网络流行"},
    {typeId: 2, typeName: "娱乐用语"},
    {typeId: 3, typeName: "游戏动漫"},
    {typeId: 4, typeName: "人文历史"},
    {typeId: 5, typeName: "生活用语"}],
    dictType:-1

  },

  dictType:function(e){
    console.log(e.currentTarget.dataset.id )
    this.setData({
      dictType:e.currentTarget.dataset.id
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})