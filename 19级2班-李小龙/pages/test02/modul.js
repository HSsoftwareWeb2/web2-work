/** 
 * 
 * JS中创建一个模块
 * 
 */
function sayHello(name) {
  console.log(`hello ${name}!`)
}


function open(fileName) {
  console.log(`正在打开 ${fileName}文件`)
  console.log(`正在执行 ${fileName}文件`),
  console.log(`正在关闭 ${fileName}文件`)
}

module.exports.sayHello=sayHello
module.exports.open=open