// pages/test02/test02.js
/** 
 * 引入模块
 * 
*/
const util = require('../test02/modul.js')
Page({
  /** 
   * 使用模块
   * 
  */
hello:function(){
util.sayHello("李小龙")

},
openFile:function(){
util.open("txt文本文档")

},


show:function(){
  console.log("监听事件")
  
  },
  /**
   * 页面的初始数据
   */
  data: {
      num:3,
      strArray:[{name:"cxy",age:15},{name:"lxl",age:18}],
      textvalue:123456,
      pingming:3,
      wodi:1
  },
  /** 自定义组件
   * 
   */
  pingmingLower:function(e){
      this.setData({
        pingming:e.detail.num,
        wodi:this.data.wodi+1
      })
  },

  pingmingAdd:function(e){
    this.setData({
      pingming:e.detail.num,
      wodi:this.data.wodi-1
    })
},

test2:function () {
         //console.log(this.data.num),
    //俩种修改方式推荐setData
    //   1. this.data.num=7,
    console.log(this.data.num)
    this.setData({
      num:7
  })
    console.log(this.data.num)
 },
 test1:function () {
  console.log("父组件")
 },

 /**
  * 测试页面跳转
  */
 reLaunch :function () {
   wx.reLaunch({
     url: '../whoIsTheUndercover/whoIsTheUndercover',
   })
 },
 jump:function () {
  wx.reLaunch({
    url: '../whoIsTheUndercover/whoIsTheUndercover',
  })
},
/**
 * 
 *  双向绑定值
 */
getText:function () {
  console.log(this.data.textvalue)
},

/**
 * 自定义属性
 * 
 */
mypro:function (e) {
  console.log(e.currentTarget.dataset.mypro) 
},
protest:function (e) {
  console.log(e.currentTarget.dataset.protest)
},



  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    console.log("监听页面显示")
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
      console.log("监听页面隐藏")
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },
  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})