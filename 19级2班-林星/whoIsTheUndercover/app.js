//app.js
App({
  onLaunch: function () {
    // 展示本地存储能力
    var logs = wx.getStorageSync('logs') || []
    logs.unshift(Date.now())
    wx.setStorageSync('logs', logs)

    // 登录
    wx.login({
      success: res => {
        // 发送 res.code 到后台换取 openId, sessionKey, unionId
        console.log(res.code);
        wx.request({
          url: 'http://127.0.0.1:8080/game/api/v1/openids?code='+res.code,
          // url:'https://api.weixin.qq.com/sns/jscode2session?appid='+'wx33b6ab6af27221fb'+'&secret='+'5beeeb401fb7f58a73167d0f1439703e'+'&js_code='+res.code+'&grant_type='+'authorization_code',
          method:"GET",
          success:result=>{
            console.log(result);
            if(result.data.code == 0){
              
              this.globalData.openId=result.data.data.openid;
            }
            console.log(this.globalData.openId)
          },
          fail(res){
            //console.log("请求失败")
          }
        })
      }
    })
    // 获取用户信息
    wx.getSetting({
      success: res => {
        if (res.authSetting['scope.userInfo']) {
          // 已经授权，可以直接调用 getUserInfo 获取头像昵称，不会弹框
          wx.getUserInfo({
            success: res => {
              // 可以将 res 发送给后台解码出 unionId
              this.globalData.userInfo = res.userInfo
              //console.log(this.globalData.userInfo)
              // 由于 getUserInfo 是网络请求，可能会在 Page.onLoad 之后才返回
              // 所以此处加入 callback 以防止这种情况
              if (this.userInfoReadyCallback) {
                this.userInfoReadyCallback(res)
              }
            }
          })
        }else{
          wx.navigateTo({
            url:'/pages/auth/auth',
          })
        }
      }
    })
  },
  globalData: {
    userInfo: null,
    openId:null,
    typeId:null,
    typeName:null,
    authorCode:null,
    roomId:123
    //avatarUrl:'ad/touxaing1.png',
    //nickName:'mao'
  }
})