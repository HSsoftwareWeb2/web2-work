// components/test/test.js
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    num:{
      type: Number,
      value:''
    },
    max:{
      type: Number,
      value:''
    },
    min:{
      type: Number,
      value:''
    }
  },

  /**
   * 组件的初始数据
   */
  data: {

  },

  /**
   * 组件的方法列表
   */
  methods: {
    jia:function(){
      // console.log(this.data.pingminNum),
      if(this.data.num < this.data.max){
        this.setData({
          num:this.data.num+1
        })
        this.triggerEvent("upper",{num:this.data.num})
      }
    },
    jian:function(){
      if(this.data.num > this.data.min){
        this.setData({
          num:this.data.num-1
        })
        this.triggerEvent("lowwer",{num:this.data.num})
      }
    },
  }
})
