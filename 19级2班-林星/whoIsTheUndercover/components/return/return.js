// components/return/return.js
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    num:{
      type:Number,
      value:0
    },
    gameStatus:{
      type:Number,
      value:0
    }
  },

  /**
   * 组件的初始数据
   */
  data: {

  },
  
  /**
   * 组件的方法列表
   */
  methods: {
    //是否退出房间
    isReturn:function(){
      this.triggerEvent({gameStatus:this.data.gameStatus})
      if(this.data.gameStatus == 1){
        this.setData({
          num:1
        })
      }else{
        this.back();
      }
      
    },
    doExit:function(){
      var that = this;
      wx.showToast({
        title: '已退出房间',
      })
      that.back();
    },
    notExit:function(){
      this.setData({
        num:0
      })
    },
    back:function(){
      wx.navigateBack({
        success(){
          delta: 1;
        },
        fail(){
          wx.redirectTo({
            url: '../homePage/homePage',
          })
        }
      })
    },
  }
})
