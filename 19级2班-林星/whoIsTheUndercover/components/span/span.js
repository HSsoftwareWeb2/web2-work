// components/span/span.js
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    word:{
      type: String,
      value:''
    }
  },

  /**
   * 组件的初始数据
   */
  data: {

  },

  /**
   * 组件的方法列表
   */
  methods: {
    onLoad:function(){
      // this.triggerEvent("upper",{num:this.data.num})
      this.triggerEvent({word:this.data.word})
      this.setData({
        word:this.data.word
      })
    }
  }
})
