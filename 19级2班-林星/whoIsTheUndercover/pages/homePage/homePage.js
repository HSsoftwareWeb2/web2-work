
const comm = require('comm.js');
const app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    num:0,
    roomId:null
  },
  doRule:function(){
    wx.navigateTo({
      url: '../rule/rule',
    })
  },
  doCRoom:function(){
    wx.navigateTo({
      url: '../roomSetting/roomSetting',
    })
  },
  doShow:function(){
    this.setData({
      num:1
    })
  },
  doHide:function(){
    this.setData({
      num:0
    })
  },
  doSearch:function(){
    console.log(this.data.roomId)
    //进入房间
    wx.navigateTo({
      url: '../waittingRoom/waittingRoom?roomId='+this.data.roomId,
    })
    //玩家加入房间
    // wx.request({
    //   url: 'http://192.168.168.7/game/api/app/v1/rooms/players',
    //   data:{
    //     roomId:app.globalData.roomId,
    //     avatarUrl:app.globalData.userInfo.avatarUrl,
    //     nickName:app.globalData.userInfo.nickName,
    //     userId:app.globalData.openId
    //   },
    //   method:"POST",
    //   success(res){
    //     if(res.data.code == 0){
    //       // this.setData({
    //       //   wodiNum:res.data.wodiNum,
    //       //   pingminNum:res.data.pingminNum,
    //       //   baibanNum:res.data.baibanNum,
    //       //   hostUserId:res.data.hostUserId
    //       // })
    //       console.log("成功加入房间")
    //       wx.navigateTo({
    //          url: '../waittingRoom/waittingRoom?roomId='+res.data.roomId+"&wodiNum="+res.data.wodiNum+"&pingminNum="+res.data.pingminNum+"&baibanNum="+res.data.baibanNum+"&hostUserId="+res.data.hostUserId,
    //       })
          
    //     }
    //   },
    //   fail(res){
    //     if(res.data.code == -1){
    //       wx.redirectTo({
    //         url: '../homePage/homePage',
    //       })
    //       wx.showToast({
    //         title: '加入房间失败',
    //       })
    //       console.log("加入房间失败")
    //     }
    //   }
    // })
    //搜索房间
    // wx.request({
    //   url: 'http://192.168.168.7/game/api/app/v1/rooms/search?roomId='+this.data.roomId,
    //   method:"GET",
    //   success(res){
    //     if(res.data.code == 0){
    //       // app.globalData.roomId=res.data.roomId,
    //       wx.navigateTo({
    //         url: '../waittingRoom/waittingRoom?roomId='+res.data.roomId,
    //       })
    //     }
    //   },
    //   fail(res){
    //     console.log(res.data.msg)
    //   }
    // })
  },
  // protest:function(e){
  //   console.log(e)
  //   comm.sayHi("Hello World");
  //   console.log(e.currentTarget.dataset.mypro)
  //   console.log(this.data.roomNum)
  // },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.setData({
      num:0
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})
