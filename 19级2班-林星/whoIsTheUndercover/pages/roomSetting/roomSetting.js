// pages/roomSetting/roomSetting.js
const app = getApp();
Page({
  

  /**
   * 页面的初始数据
   */
  data: {
    // wordArr:[
    //   {typeId:1,typeName:"全部"},
    //   {typeId:2,typeName:"网络流行"},
    //   {typeId:3,typeName:"娱乐用语"},
    //   {typeId:4,typeName:"游戏动漫"},
    //   {typeId:5,typeName:"人文历史"},
    //   {typeId:6,typeName:"生活用品"},
    // ],
    wordArr:[],
    playerNum:4,
    show:-1,
    pingminNum:3,
    wodiNum:1,
    baibanNum:0,
    openId:"",
    code:1,
    userId:app.globalData.openId
  },
  showSel:function(e){
    console.log(e.currentTarget.dataset.id),
    // console.log(this.data.wordArr[this.data.show]),
    this.setData({
      show:e.currentTarget.dataset.id
    })
    // console.log(this.data.show)
  },
  //打开白板
  isOpenBaiban:function(event){
    // console.log(event);
    if(event.detail.value == true){
      this.setData({
        baibanNum:1
        // pingminNum: this.data.pingminNum - 1(非常规用)
      })
      this.reCounter(event.detail.value);
    }else if(event.detail.value == false){
      this.setData({
        baibanNum:0,
        // pingminNum: this.data.pingminNum + 1(非常规用)
      })
      this.reCounter(event.detail.value);
    }
  },
  //开启白板重新计算人数（正常规则用）
  reCounter:function(isTrue){
    if(this.data.pingminNum-1 > this.data.wodiNum){
      pingminNum: this.data.pingminNum - 1
    }
  },
  sliChange:function(e){
    var baibanNum=this.data.baibanNum;
    var newPinMing=this.data.pingming;
    var newWodi=this.data.wodiNum;
    var newSilder =e.detail.value;
    if(newSilder<=5){
      newWodi=1
    }
    if(5<newSilder){
      newWodi=2
    }
    if(9<=newSilder){
      newWodi=3
    }
    // console.log(newSilder,newWodi,baibanNum)
    newPinMing=newSilder-newWodi-baibanNum;
    this.setData({
      playerNum:newSilder,
      pingminNum:newPinMing,
      wodiNum:newWodi

    })
  },
  
  pingminUpper:function(e){
      this.setData({
        pingminNum:e.detail.num,
        wodiNum:this.data.wodiNum-1
      })
  },
  pingminLowwer:function(e){
      this.setData({
        pingminNum:e.detail.num,
        wodiNum:this.data.wodiNum+1
      })
  },
  wodiUpper:function(e){
      this.setData({
        wodiNum:e.detail.num,
        pingminNum:this.data.pingminNum-1
      })
  },
  wodiLowwer:function(e){
      this.setData({
        wodiNum:e.detail.num,
        pingminNum:this.data.pingminNum+1
      })
  },
  complete:function(){
    console.log(this.data.show)
    console.log("请求创建房间")
    if(this.data.show != -1){
      //创建房间接口
      wx.request({
        url: 'http://127.0.0.1:8080/game/api/v1/rooms',
        data:{
          wodiNum:this.data.wodiNum,
          pingminNum:this.data.pingminNum,
          baibanNum:this.data.baibanNum,
          dictType:this.data.show,
          userId:app.globalData.openId
        },
        method:"POST",
        header: {
          'content-type': 'application/x-www-form-urlencoded' 
        }, 
        success:res=>{
          console.log(res),
          
          wx.redirectTo({
            url: '../waittingRoom/waittingRoom?roomId='+res.data.data.roomId,
          })
        },
        fail(res){
          console.log("请求失败RS")
        }
      })
    }else{
      console.log("请选择词语类型")
    }
    
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    // this.complete();
    console.log(app.globalData.openId)
    //获取词语类型接口
    wx.request({
      url: 'http://127.0.0.1:8080/game/api/v1/types',
      method:"GET",
      success:res=>{
        console.log(res);
        if(res.data.code == 0){
          this.setData({
            wordArr:res.data.data
          })
        }
      },
      fail(res){
        console.log("请求失败")
      }
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})