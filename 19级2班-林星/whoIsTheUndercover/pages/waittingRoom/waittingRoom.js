// pages/waittingRoom/waittingRoom.js
const app = getApp();
var conn;//webSocket连接
var timer;//websocket心跳定时器
Page({

  /**
   * 页面的初始数据
   */
  data: {
    pingminNum:0,
    wodiNum:0,
    baibanNum:0,
    playerNum:0,
    gameStatus:0,
    userId:app.globalData.openId,
    hostUserId:123,
    // num:0,
    roomId:null,
    lastTapTime:0,
    myWord:null,
    // userInfo:[
    //   {imgUrl:"../imgs/touxiang2.png",userName:"test",isFail:false,userId:""},
    //   {imgUrl:"../imgs/touxiang2.png",userName:"test",isFail:true,userId:""},
    //   {imgUrl:"../imgs/touxiang2.png",userName:"test",isFail:false,userId:""},
    //   {imgUrl:"../imgs/touxiang2.png",userName:"test",isFail:false,userId:""},
    //   {imgUrl:"../imgs/touxiang2.png",userName:"test",isFail:false,userId:""},
    //   {imgUrl:"../imgs/touxiang2.png",userName:"test",isFail:false,userId:""},
    //   {imgUrl:"../imgs/touxiang2.png",userName:"test",isFail:false,userId:""},
    //   {imgUrl:"../imgs/touxiang2.png",userName:"test",isFail:false,userId:""},
    //   {imgUrl:"../imgs/touxiang2.png",userName:"test",isFail:false,userId:""},
    //   {imgUrl:"../imgs/touxiang2.png",userName:"test",isFail:false,userId:""},
    //   {imgUrl:"../imgs/touxiang2.png",userName:"test",isFail:false,userId:""},
    //   {imgUrl:"../imgs/touxiang2.png",userName:"test",isFail:false,userId:""},
    // ],
    userInfo:[],
    winner:{
      character:"../imgs/v_pingmin.png",
      pingminWord:"苹果",
      wodiWord:"桃子"
    }
  },
  //双击事件
  doubleClick:function(e){
    var curTime = e.timeStamp
    var lastTime = e.currentTarget.dataset.time  // 通过e.currentTarget.dataset.time 访问到绑定到该组件的自定义数据
    console.log("上一次点击时间："+lastTime)
    console.log("这一次点击时间：" + curTime)
    console.log('------------------------------');
    if (curTime - lastTime > 0) {
      if (curTime - lastTime < 300) {//是双击事件
        console.log("挺快的双击，用了：" + (curTime - lastTime))
        this.vote(e.currentTarget.dataset.userid);
      }
      
    }
    this.setData({
      lastTapTime: curTime
    })

  },
  //投票方法
  vote:function(votedId){
    if(this.data.gameStatus != 1){
      wx.showToast({
        title: '游戏尚未进行',
      })
    }else if(this.data.hostUserId == userId){
      wx.request({
        url: 'http://127.0.0.1:8080/game/api/app/v1/rooms/game/votes',
        data:{
          roomId:this.data.roomId,
          votedUserId:votedId,
          seqNo:this.data.seqNo,
          hostUserId:this.data.hostUserId
        },
        method:"POST",
        header: {
          'content-type': 'application/x-www-form-urlencoded' 
        },      
        success(res){
          if(res.data.code == 0){
            wx.showToast({
              title: '投票成功',
            })
          }
        },
        fail(res){
          if(res.data.code != 0){
            console.log(res.msg)
            wx.showToast({
              title: '投票失败，请重新投票',
            })
          }
        }
      })
    }else{
      wx.showToast({
        title: '只有房主有权限投票',
      })
    }
    
  },
  //开始游戏
  startGame:function(){
    wx.request({
      url: 'http://127.0.0.1:8080/game/api/app/v1/rooms/games',
      data:{
        roomId:this.data.roomId,
        userId:hostUserId
      },
      method:"POST",
      header: {
        'content-type': 'application/x-www-form-urlencoded' 
      },      
      success(res){
        if(res.data.code == 0){
          gameStatus:1;
          seqNo:res.data.seqNo
        }
      },
      fail(res){
        if(res.data.code != 0){
          console.log("请求失败")
        }
      }
    })
  },
  //玩家退出房间方法
  exitRoom:function(){
    wx.redirectTo({
      url: '../homePage/homePage',
    })
  },
  closeOvBorad:function(){
    this.setData({
      gameStatus:0
    })
  },
  joinRoom:function(res){
    this.setData({
      userInfo:res.data.users
    })
    console.log(this.data.userInfo)
  },
  gaming:function(res){
    this.setData({
      userInfo:res.data.users
    })
  },
  //游戏结束方法
  gameOver:function(msg){
    if(msg.data.gameStatus==0){
      this.setData({
        winner:msg.data.winner,
        gameStatus:msg.data.gameStatus
      })
      wx.showToast({
        title: '游戏已结束',
      })
    }
  },
  //房主退出房间方法
  playerExit:function(){
    if(this.data.hostUserId == userId){
      wx.request({
        url: 'http://127.0.0.1:8080/game/api/app/v1/rooms/?roomId='+this.data.roomId+"&userId="+this.data.userId,
        method:"DELETE",
        success:res=>{
          if(res.data.code == 0){
            console.log(res.data.msg)
          }
        },
        fail(res){
          if(res.data.code == -1){
            console.log(res.data.msg)
          }
        }
      })
    }
  },
  //玩家加入房间
  pJoinroom:function(res){
    // console.log("jiaru")
    // console.log("用户userId为："+this.data.userId)
    // console.log(app.globalData.userInfo.nickName)
    wx.request({
      url: 'http://127.0.0.1:8080/game/api/v1/rooms/players',
      data:{
        roomId:this.data.roomId,
        avatarUrl:app.globalData.userInfo.avatarUrl,
        nickName:app.globalData.userInfo.nickName,
        userId:app.globalData.openId
      },
      method:"POST",
      header: {
        'content-type': 'application/x-www-form-urlencoded' 
      },  
      success:(res)=>{
        console.log(res)
        if(res.data.code == 0){
          this.setData({
            wodiNum:res.data.data.wodiNum,
            pingminNum:res.data.data.pingminNum,
            baibanNum:res.data.data.baibanNum,
            hostUserId:res.data.data.userId
          })
          console.log("房主id为"+this.data.hostUserId)
        }else if(res.data.code != 0){
          wx.showToast({
            title: '房间不存在，或已满',
          })
          // wx.redirectTo({
          //   url: '../homePage/homePage',
          // })
        }
      },
      fail(res){
        if(res.data.code == -1){
          wx.redirectTo({
            url: '../homePage/homePage',
          })
          wx.showToast({
            title: '加入房间失败',
          })
          console.log("加入房间失败")
        }
      }
    })
  },

  //刷新玩家词语
  flashMyWord:function(){
    for(var i = 0 ; i <= this.data.userInfo.length ; i++){
      if(this.data.userInfo[i].userId == this.data.userId){
        this.setData({
          myWord:this.data.userInfo[i].word
        })
      }
    }
  },

  //关闭房间
  closeRoom:function(){
    wx.showToast({
      title: '房主退出房间，房间即将关闭',
    })
    setTimeout(function(){
      wx.redirectTo({
        url: '../homePage/homePage',
      })
    },2000)
  },
  //接收webSocket服务器信息
  wxOnMessage:function(conn){
    conn.onMessage((res)=>{
      console.log("msg中的内容时"+res);
      var msg = JSON.parse(res.data);
      console.log("webSocket消息")
      console.log(msg)
      if(msg.msgType == "ENTER_ROOM"){
        this.joinRoom(msg);
      }else if(msg.msgType == "START_GAME"){
        gaming(msg);
        this.flashMyWord();
      }else if(msg.msgType == "VOTE"){
        this.gaming(msg);
      }else if(msg.msgType == "GAME_OVER"){
        this.gaming(msg);
        this.gameOver(msg);
      }else if(msg.msgType == "EXIT"){
        this.gaming(msg);
      }else if(msg.msgType == "LEAVE_ROOM"){
        this.closeRoom();
      }
    })
  },
  //webSocket心跳
  sendMsg:function(){
    conn.send({
      data:"keep touch",
      success:function(res){
        console.log("发送成功");
      }
    })
  },
  
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    
    //填充roomId
    this.setData({
      roomId:options.roomId,
      // wodiNum:options.wodiNum,
      // pingminNum:options.pingminNum,
      // baibanNum:options.baibanNum,
      // hostUserId:options.hostUserId,
      userId:app.globalData.openId
      
    })
    
    //玩家加入房间(页面加载时)
    // this.pJoinroom();

    //webSoket创建conn连接
    conn = wx.connectSocket({
      url: 'ws://127.0.0.1:8080/game/'+app.globalData.openId,
      header:{
        'content-type': 'application/json'
      },
      // protocols: ['protocol1']
    })
    //webSoket监听conn连接
    conn.onOpen(res=>{
      // var that = this;
      console.log("webSoket连接打开");
      timer = setInterval(this.sendMsg,1000*60);//每隔60秒发送websocket消息保持连接不中断
      this.wxOnMessage(conn)
      this.pJoinroom()
    })    

    //监听web关闭并关闭timer计时器
    conn.onClose(function(res){
      clearInterval(timer)
    })

    
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    // conn.onMessage({})
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    //退出房间
    this.playerExit();
    conn.close(function(){
      code: 0
    })
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})